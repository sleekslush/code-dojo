import sys
from StringIO import StringIO


class DragonNode(object):
    s = None
    a = None
    ai = None
    b = None
    bi = None

    def get_instructions(self):
        sio = StringIO()

        if self.ai:
            sio.write(self.s[:self.ai])

        if self.a:
            sio.write(self.a.get_instructions())

        if self.bi:
            sio.write(self.s[self.ai+1:self.bi])

        if self.b:
            sio.write(self.b.get_instructions())

        if self.bi:
            sio.write(self.s[self.bi+1:])

        return sio.getvalue()

class DragonRoot(DragonNode):
    s = 'Fa'
    ai = 1


class DragonA(DragonNode):
    s = 'aRbFR'
    ai = 0
    bi = 2


class DragonB(DragonNode):
    s = 'LFaLb'
    ai = 2
    bi = 4


def main(n):
    root = DragonRoot()
    nodes = [root]

    for i in range(n):
        new_nodes = []

        for node in nodes:
            node.a = DragonA()
            new_nodes.append(node.a)

            if node.bi is not None:
                node.b = DragonB()
                new_nodes.append(node.b)

        nodes = new_nodes

    print root.get_instructions()

if __name__ == '__main__':
    main(int(sys.argv[1]))
