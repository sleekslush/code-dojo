from collections import defaultdict

rows = [
# id    uuid    ip
    (1, 'abc', '111'),
    (1, 'def', '111'),
    (1, 'abc', '222'),
    (2, 'abc', '333'), # related to 1 by uuid
    (3, 'efg', '222'), # related to 1 ip, 5 by uuid
    (4, 'xyz', '555'), # unique
    (5, 'efg', '666'), # related to 3 by uuid
]

expected =     {
    1: {2, 3},
    2: {1},
    3: {1, 5},
    4: set(),
    5: {3}
}

def find_related(rows):
    """ returns a dict, with a list of related ids per unique id rows """

    # build a lookup table
    index = defaultdict(set)
    for id, uuid, ip in rows:
        for key in (('uuid', uuid), ('ip', ip)):
            index[key].add(id)

    # populate the results
    results = defaultdict(set)
    for key, ids in index.iteritems():
        ids = list(ids)
        for i in range(len(ids)):
            results.setdefault(ids[i], set())
            for id in ids[0:i] + ids[i+1:]:
                results[ids[i]].add(id)
    return results

def get_rows():
    with open('/Users/craig/Downloads/test_data.csv') as f:
        for line in f:
            yield line.strip().split(',')

find_related(get_rows())

#assert(find_related(rows) == expected)
