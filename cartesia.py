def exactlyN(n, directions):
    if not directions:
        return n == 0

    x, y = start = 0, 0

    for i, direction in enumerate(directions):
        if i > n:
            return False

        if direction == 'n':
            y += 1
        elif direction == 's':
            y -= 1
        elif direction == 'e':
            x += 1
        elif direction == 'w':
            x -= 1

    return (i + 1) == n and (x, y) == start


if __name__ == '__main__':
    assert exactlyN(0, [])
    assert not exactlyN(0, ['n'])
    assert not exactlyN(1, [])
    assert not exactlyN(1, ['n'])
    assert not exactlyN(4, ['n', 's'])
    assert exactlyN(10, ['n', 'e', 's', 'w', 'w', 'n', 'e', 's', 'e', 'w'])
    assert not exactlyN(10, ['n', 'e', 's', 'w', 'w', 'n', 'e', 's', 'e', 'w', 's'])
    assert exactlyN(8, ['n', 'e', 's', 'w', 'w', 'n', 'e', 's'])
    assert not exactlyN(8, ['n', 'e', 's', 'w', 'w', 'n', 'e', 'n'])
