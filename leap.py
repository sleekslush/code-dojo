def leap(year):
    return (year % 4 == 0) and ((year % 400 == 0) or (year % 100 != 0))


if __name__ == '__main__':
    assert not leap(1997)
    assert leap(1996)
    assert not leap(1900)
    assert leap(2000)
