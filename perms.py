#import itertools


#def p(l):
#    return [''.join(t) for t in itertools.permutations(l, len(l))]


def perm(s):
    def p(s):
        if len(s) <= 1:
            yield s
        else:
            for perm in p(s[1:]):
                for i in xrange(len(s)):
                    yield perm[:i] + s[0:1] + perm[i:]

    return list(p(s))

if __name__ == '__main__':
    assert perm('ab') == ['ab', 'ba']
