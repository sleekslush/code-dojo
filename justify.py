def extra_spaces(line, k):
    return k - sum(map(len, line)) - (len(line) - 1)

def justify_line(line, extra_spaces):
    separator = ' ' + (' ' * (extra_spaces / (len(line) - 1)))
    result = [[word, separator] for word in line]
    if extra_spaces % 2 != 0:
        result[0][1] += ' '
    return ''.join(word + separator for word, separator in result).strip()

def justify(words, k):
    result = []
    line = []

    for word in words:
        chars_left = extra_spaces(line, k)
        
        if len(word) <= chars_left:
            line.append(word)
        else:
            result.append(justify_line(line, chars_left))
            line = [word]

    chars_left = extra_spaces(line, k)
    result.append(justify_line(line, chars_left))

    return result

if __name__ == '__main__':
    words = ['the', 'quick', 'brown', 'fox', 'jumps', 'over', 'the', 'lazy', 'dog']

    expected_result = [
            'the  quick brown',
            'fox  jumps  over',
            'the   lazy   dog',
            ]

    actual_result = justify(words, k=16)

    assert actual_result  == expected_result
