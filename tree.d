module tree;

import std.conv : to;
import std.range : repeat, take;
import std.stdio : writeln;

version(unittest)
{
    import std.algorithm : equal;
    import std.range : takeNone;
}

void main(string[] args)
{
    christmasTree(args[1].to!uint);
}

auto christmasTree(in uint height)
{
    for (auto level = 1; level <= height; level++)
    {
        writeln(height.getSpaces(level), level.getStars);
    }
}

auto getSpaces(in uint height, in uint level)
in
{
    assert(level > 0 && level <= height);
}
body
{
    return ' '.repeat.take(height - level);
}

unittest
{
    auto noSpaces = [' '].takeNone;
    assert(equal(getSpaces(1, 1), noSpaces));
    assert(equal(getSpaces(5, 1), ' '.repeat.take(4)));
    assert(equal(getSpaces(5, 4), [' ']));
    assert(equal(getSpaces(100, 100), noSpaces));
}

auto getStars(in uint level)
in
{
    assert(level > 0);
}
body
{
    return '*'.repeat.take(level * 2 - 1);
}

unittest
{
    assert(equal(getStars(1), ['*']));
    assert(equal(getStars(5), '*'.repeat(9)));
    assert(equal(getStars(100), '*'.repeat(199)));
}
