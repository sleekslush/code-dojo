from itertools import count

def sorted_cube(i):
    return ''.join(sorted(n for n in str(i)))

def cubes():
    for i in count(1):
        yield i, i ** 3

def main():
    cube_strs = {}

    for i, cube in cubes():
        cube_str = sorted_cube(cube)
        if cube_str not in cube_strs:
            cube_strs[cube_str] = [0, []]

        cube_counts = cube_strs[cube_str]
        cube_counts[0] += 1
        cube_counts[1].append(i)

        if cube_counts[0] == 5:
            print cube_counts[1][0]
            break

if __name__ == '__main__':
    main()
