import sys


def christmasTree(height):
    for n in range(1, height + 1):
        spaces = height - n - 1
        if spaces >= 0:
            print ' ' * spaces,

        stars = (n * 2) - 1
        print '*' * stars


if __name__ == '__main__':
    christmasTree(int(sys.argv[1]))
