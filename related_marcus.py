import itertools
from collections import defaultdict

rows = [
# id    uuid    ip
    (1, 'abc', '111'),
    (1, 'def', '111'),
    (1, 'abc', '222'),
    (2, 'abc', '333'), # related to 1 by uuid
    (3, 'efg', '222'), # related to 1 ip, 5 by uuid
    (4, 'xyz', '555'), # unique
    (5, 'efg', '666'), # related to 3 by uuid
]

expected =     {
    1: [2, 3],
    2: [1],
    3: [1, 5],
    4: [],
    5: [3]
}

def find_related(rows):
    """ returns a dict, with a list of related ids per unique id rows """

    related = defaultdict(set)
    final = defaultdict(list)
    for id, uuid, ip in rows:
        related[uuid].add(id)
        related[ip].add(id)
    for s in related.itervalues():
        if len(s) == 1:
            continue
        else:
            for k, v in itertools.combinations(s, 2):
                final[k].append(v)
                final[v].append(k)
    return final

def get_rows():
    rows = []
    with open('/Users/craig/Downloads/test_data.csv') as f:
        for line in f:
            rows.append(line.strip().split(','))
    return rows

find_related(get_rows())

#assert(find_related(rows) == expected)
