def f(n, m):
    ps = lambda n: (n * (n+1)) / 2
    return (n//m * ps(m-1)) + ps(n%m)


if __name__ == '__main__':
    assert f(10, 5) == 20
    assert f(20, 20) == 190
    assert f(15, 10) == 60
    assert f(123456789, 69) == 4197530541
