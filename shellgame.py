def find_the_ball(pos, swaps):
    for a, b in swaps:
        if a == pos:
            pos = b
        elif b == pos:
            pos = a

    return pos


if __name__ == '__main__':
    assert find_the_ball(0, [(0, 1), (2, 1), (0, 1)]) == 2
