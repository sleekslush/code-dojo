def max_subarray(a, k):
    l = len(a)
    return [max(a[i:i+k]) for i in xrange(l-k+1)]

if __name__ == '__main__':
    a = [10,5,2,7,8,7]
    assert list(max_subarray(a, 3)) == [10,7,8,8]
    assert list(max_subarray(a, 2)) == [10,5,7,8,8]
