var Dictionary = (function() {
  function Dictionary(words) {
    this.tree = buildTree(words);
  };

  Dictionary.prototype.showTree = function() {
    console.log(JSON.stringify(this.tree));
  };

  Dictionary.prototype.getMatchingWords = function(search) {
    var result = getMatchingWords(search, 0, this.tree, []);
    //console.log(result);
    return result;
  };

  return Dictionary;

  function getMatchingWords(search, i, tree, results) {
    var curr = tree;

    for (; i < search.length; i++) {
      var letter = search[i];

      if (letter == '?') {
        for (var l in curr.l) {
          if (curr.l.hasOwnProperty(l)) {
            if (!((search.length - i) in curr.s)) {
              break;
            }
            getMatchingWords(search, i+1, curr.l[l], results);
          }
        }

        return results;
      } else if (!(letter in curr.l)) {
        return results;
      } else {
        curr = curr.l[letter];
      }
    }

    if (curr.w) {
      results.push(curr.w);
    }

    return results;
  }

  function buildTree(words) {
    var tree = defaultNode();

    for (var i = 0; i < words.length; i++) {
      var word = words[i];
      var curr = tree;

      for (var j = 0; j < word.length; j++) {
        var letter = word[j];

        if (!(letter in curr.l)) {
          curr.l[letter] = defaultNode();
        }

        curr.s[word.length - j] = true;

        curr = curr.l[letter];
      }

      curr.w = word;
    }

    return tree;
  }

  function defaultNode() {
    return {'l': {}, 's': {}}
  }
})();

var list = ['banana', 'apple', 'papaya', 'cherry'];
for (var i=0;i<10000;i++) {
    var randString = (function(){var a='';for (var i=0;i<Math.floor(Math.random()*8)+5;i++) { a+='x'; } return a;})().replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
    list.push(randString);
}


// Benchmark
console.time('test');

var fruits = new Dictionary(list);

for (var i = 0; i < 200000; i++) {
    fruits.getMatchingWords('lemon');
    fruits.getMatchingWords('cherr??');
    fruits.getMatchingWords('?a?a?a');
    fruits.getMatchingWords('??????');    
}

console.timeEnd('test');
