def snail(array):
    result = []

    def slime(x, width, height):
        if x >= height or x + 1 > width:
            return

        for i in xrange(x, width):
            result.append(array[x][i])

        for i in xrange(x + 1, height - 1):
            result.append(array[i][width - 1])

        for i in xrange(width - 1, x, -1):
            result.append(array[height - 1][i])

        for i in xrange(height - 1, x, -1):
            result.append(array[i][x])

        slime(x + 1, width - 1, height - 1)

    slime(0, len(array[0]), len(array))

    return result


if __name__ == '__main__':
    assert [1,2,3,6,9,8,7,4,5] == \
            snail([
                [1,2,3],
                [4,5,6],
                [7,8,9]
                ])

    assert [1,2,3,4,4,3,2,1] == \
            snail([
                [1,2,3,4],
                [1,2,3,4]
                ])

    assert [1] == snail([[1]])

    assert [1,2,3,4,4,4,4,4,3,2,1,1,1,1,2,3,3,3,2,2] == \
            snail([
                [1,2,3,4],
                [1,2,3,4],
                [1,2,3,4],
                [1,2,3,4],
                [1,2,3,4]
                ])

    assert [1,2,3,6,9,5,5,5,7,4,5,8] == \
            snail([
                [1, 2, 3],
                [4, 5, 6],
                [7, 8, 9],
                [5, 5, 5]
                ])
