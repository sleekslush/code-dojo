module checkboard;

import std.conv;
import std.stdio;

void main(string[] args)
{
    if (args.length < 2)
    {
        writefln("Usage: %s <num>", args[0]);
        return;
    }

    auto size = to!int(args[1]);
    auto isEven = (size % 2 == 0);

    auto cells = ["■", "□"];
    auto cellIndex = isEven ? 1 : 0;

    for (auto x = 0; x < size; x++)
    {
        for (auto y = 0; y < size; y++)
        {
            writef("%s ", cells[cellIndex]);
            cellIndex = 1 - cellIndex;
        }

        if (isEven)
        {
            cellIndex = 1 - cellIndex;
        }

        writeln;
    }
}
