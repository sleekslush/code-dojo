import random
from bisect import bisect_left
from collections import OrderedDict

def get_distro_fn(d):
    lookup = OrderedDict()
    for k, v in sorted(d.iteritems(), key=lambda (k, v): (v, k)):
        lookup.setdefault(v, []).append(k)

    keys = lookup.keys()
    tot_keys = len(keys)

    def dist_fn():
        i = random.randint(1, 100)
        pos = bisect_left(keys, i)

        if pos == 0:
            key = keys[0]
        elif pos < tot_keys - 1:
            key = keys[pos + 1]
        else:
            key = keys[-1]

        return random.choice(lookup[key])

    return dist_fn

if __name__ == '__main__':
    fn = get_distro_fn({'z': 20, 'a': 10, 'b': 15, 'c': 20, 'd': 25, 'e': 30})

    for i in xrange(10):
        print fn()
